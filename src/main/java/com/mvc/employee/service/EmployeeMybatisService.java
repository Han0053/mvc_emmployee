package com.mvc.employee.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mvc.employee.bean.Employee;
import com.mvc.employee.calculation.Caculate;
import com.mvc.employee.mapper.UserMapper;

@Service
public class EmployeeMybatisService implements UserService {

	@Autowired
	public UserMapper userMapper;

	public Employee findById(int id) {
		return userMapper.findById(id);
	}

	public int deleteById(int id) {
		return userMapper.deleteById(id);
	}

	public int create(Employee employee) {
		
		Caculate cal = new Caculate();
		
		double bmi = cal.bmi(employee.getHeight(), employee.getWeight());
		employee.setBmi(bmi);
		return userMapper.create(employee);
	}

	public int updateById(Employee employee) {
		
		Caculate cal = new Caculate();
		
		double bmi = cal.bmi(employee.getHeight(), employee.getWeight());
		employee.setBmi(bmi);
		return userMapper.updateById(employee);
	}

	public boolean batchInsert(List<String> list) {
		List<Employee> empList = new ArrayList<Employee>();

		for (String str : list) {
			String[] tmpAry = str.split(",");

			Employee emp = new Employee();

			emp.setId(Integer.parseInt(tmpAry[0]));
			emp.setHeight(Integer.parseInt(tmpAry[1]));
			emp.setWeight(Integer.parseInt(tmpAry[2]));
			emp.setEngName(tmpAry[3]);
			emp.setChiName(tmpAry[4]);
			emp.setPhone(tmpAry[5]);
			emp.setEmail(tmpAry[6]);
			emp.setBmi(Double.parseDouble(tmpAry[7]));
			empList.add(emp);
		}
		return userMapper.batchInsert(empList);
	}

	public String listToCSV() {
		StringBuilder sb = new StringBuilder();

		for (Employee emp : userMapper.download()) {
			sb.append(emp.getId()).append(",");
			sb.append(emp.getHeight()).append(",");
			sb.append(emp.getWeight()).append(",");
			sb.append(emp.getEngName()).append(",");
			sb.append(emp.getChiName()).append(",");
			sb.append(emp.getPhone()).append(",");
			sb.append(emp.getEmail()).append(",");
			sb.append(emp.getBmi()).append(",");
			sb.append(emp.getCreateTime()).append(",");
			sb.append(emp.getUpdateTime()).append("\n");
		}
		return sb.toString();
	}

	@Override
	public List<Employee> download() {
		// TODO Auto-generated method stub
		return null;
	}
}
