package com.mvc.employee.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.mvc.employee.bean.Employee;
import com.mvc.employee.calculation.Caculate;
import com.mvc.employee.repository.EmployeeRepository;

@Service
@CacheConfig(cacheNames = "empCache")
public class EmployeeService {
	
	@Autowired
	public EmployeeRepository repo;
	
	@Cacheable(key = "#id")
	public Employee findById(int id) {
		return repo.queryId(id);
	}
	
	@CacheEvict(key = "#id")
	public int deleteById(int id) {
		return repo.deleteId(id);
	}
	
	@CachePut(key = "#employee.id")
	public Employee updateById(Employee employee) {
		Caculate cal = new Caculate();
		
		double bmi = cal.bmi(employee.getHeight(), employee.getWeight());
		employee.setBmi(bmi);
		return repo.update(employee);
	}
	
	@CachePut(key = "#employee.id")
	public Employee create(Employee employee) {
		
		Caculate cal = new Caculate();
		
		double bmi = cal.bmi(employee.getHeight(), employee.getWeight());
		employee.setBmi(bmi);
		return repo.create(employee);
	}
	
	public boolean batchInsert(List<String> list) {
		
		List<Employee> empList = new ArrayList<Employee>();
		
		for(String str : list) {
			String[] tmpAry = str.split(",");
			
			Employee emp = new Employee();
			
			emp.setId(Integer.parseInt(tmpAry[0]));
			emp.setHeight(Integer.parseInt(tmpAry[1]));
			emp.setWeight(Integer.parseInt(tmpAry[2]));
			emp.setEngName(tmpAry[3]);
			emp.setChiName(tmpAry[4]);
			emp.setPhone(tmpAry[5]);
			emp.setEmail(tmpAry[6]);
			emp.setBmi(Double.parseDouble(tmpAry[7]));
			
			empList.add(emp);
		}
		return repo.batchInsert(empList);
	}
	
	public String listToCSV() {
		StringBuilder sb = new StringBuilder();
		for (Employee emp : repo.queryAll()) {
			sb.append(emp.getId()).append(",");
			sb.append(emp.getHeight()).append(",");
			sb.append(emp.getWeight()).append(",");
			sb.append(emp.getEngName()).append(",");
			sb.append(emp.getChiName()).append(",");
			sb.append(emp.getPhone()).append(",");
			sb.append(emp.getEmail()).append(",");
			sb.append(emp.getBmi()).append(",");
			sb.append(emp.getCreateTime()).append(",");
			sb.append(emp.getUpdateTime()).append("\n");
		}
		return sb.toString();
	}
	
	public List<Employee> queryAll(){
		
		List<Employee> result = repo.queryAll();
		
		if(result.size() > 0) {
            return result;
        } else {
            return new ArrayList<Employee>();
        }
	}
	
	public boolean checkPhoneSev(String phone) {
		return repo.checkPhoneDao(phone);
	}
	
	public boolean checkEmailSev(String email) {
		return repo.checkEmailDao(email);
	}
}
