package com.mvc.employee.service;

import java.util.List;
import com.mvc.employee.bean.Employee;

public interface BaseService<T> {
	
	Employee findById(int id);
	
	int deleteById(int id);
	
	int create(Employee employee);
	
	int updateById(Employee employee);
	
	boolean batchInsert(List<String> list);
	
	List<Employee> download();
}
