package com.mvc.employee.service;

import com.mvc.employee.bean.Employee;

public interface UserService extends BaseService<Employee> {}
