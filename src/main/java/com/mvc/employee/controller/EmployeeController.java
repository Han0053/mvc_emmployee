package com.mvc.employee.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.mvc.employee.bean.Employee;
import com.mvc.employee.service.EmployeeMybatisService;
import com.mvc.employee.service.EmployeeService;

@RequestMapping("/employee")
@RestController
public class EmployeeController {

	private static final Logger logger = LogManager.getLogger(EmployeeMybatisService.class);

	@Autowired
	public EmployeeService employeeService;

	@GetMapping("/{id}")
	public Employee findById(@PathVariable int id) {
		return employeeService.findById(id);
	}

	@DeleteMapping("/delete/{id}")
	public int deleteById(@PathVariable int id) {
		return employeeService.deleteById(id);
	}

	@PostMapping("/create")
	public Employee create(@Valid @RequestBody Employee employee) {
		return employeeService.create(employee);
	}

	@PutMapping("/update/{id}")
	public Employee updateById(@Valid @PathVariable int id, @RequestBody Employee employee) {
		employee.setId(id);
		return employeeService.updateById(employee);
	}

	@PostMapping("/batchInsert")
	public boolean batchInsert(@RequestParam("file") MultipartFile file) {

		List<String> list = new ArrayList<String>();

		if (file.isEmpty()) {
			return false;
		}

		try (BufferedReader read = new BufferedReader(
				new InputStreamReader(file.getInputStream(), StandardCharsets.UTF_8))) {

			String line;
			while ((line = read.readLine()) != null) {
				list.add(line);
			}
		} catch (FileNotFoundException e) {
			 logger.error("file: {}, error: {}", file.getName(), e);
		} catch (IOException e) {
			 logger.error("file: {}, error: {}", file.getName(), e);
		}
		return employeeService.batchInsert(list);
	}

	@GetMapping("/download")
	public ResponseEntity<byte[]> download(HttpServletResponse response) {
		
		String empCsv = employeeService.listToCSV();
		byte[] empByt = empCsv.getBytes();
		String fileName = "employeeJDBC.csv";
		HttpHeaders httpHeader = new HttpHeaders();
		httpHeader.setContentLength(empByt.length);
		httpHeader.setContentType(new MediaType("text", "csv"));
		httpHeader.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
		return new ResponseEntity<byte[]>(empByt, httpHeader, HttpStatus.OK);
	}
	
	@GetMapping("/check/phone")
	public boolean CheckPhone(@RequestParam String phone) {
		return employeeService.checkPhoneSev(phone);
	}
	
	@GetMapping("/check/email")
	public boolean CheckEmail(@RequestParam String email) {
		return employeeService.checkEmailSev(email);
	}
}
