package com.mvc.employee.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.mvc.employee.bean.Employee;
import com.mvc.employee.service.EmployeeService;

@Controller
@RequestMapping("/view")
public class ViewController
{
    @Autowired
    private EmployeeService empService;
 
    @GetMapping("/home")
    public String getAllEmployees(Model model)
    {
        List<Employee> empList = empService.queryAll();
 
        model.addAttribute("employees", empList);
        return "index";
    }
    
    @GetMapping("/userPage")
    public String getAllEmployeesForUser(Model model)
    {
        List<Employee> empList = empService.queryAll();
 
        model.addAttribute("employees", empList);
        return "indexForUser";
    }
    
    @GetMapping(path= {"/employee"})
    public String findById(@RequestParam (value = "query", required = false) int id, Model model) 
    {
    	Employee emp = empService.findById(id);
    	model.addAttribute("query", emp);
    	return "query-emp";
    }
    
    @GetMapping(path = {"/createForm"})
    public String createForm(Model model)
    {
        model.addAttribute("emp", new Employee());
        return "create-emp";
    }
 
    @PostMapping(path = "/create")
    public String createEmployee(@Valid Employee emp, BindingResult result, Model model)
    {
    	empService.create(emp);
    	model.addAttribute("employees", empService.queryAll());
        return "redirect:/view/home";
    }
 
    @GetMapping(path = {"/update/{id}"})
    public String updateForm(@PathVariable("id") int id, Model model)
    {
        model.addAttribute("emp", empService.findById(id));
        return "update-emp";
    }
    
    @PostMapping("/update")
	public String updateEmployee(@Valid Employee emp, BindingResult result, Model model)
    {
		empService.updateById(emp);
		model.addAttribute("employees", empService.queryAll());
		return "redirect:/view/home";
	}

     
    @GetMapping(path = "/delete/{id}")
    public String deleteEmployeeById(@PathVariable("id") int id, Model model)
    {
    	empService.deleteById(id);
    	model.addAttribute("employee", empService.queryAll());
        return "redirect:/view/home";
    }
}
