package com.mvc.employee.mapper;

import java.util.List;
import com.mvc.employee.bean.Employee;

public interface UserMapper {
	Employee findById(int id);
	
	int deleteById(int id);
	
	int create(Employee employee);
	
	int updateById(Employee employee);
	
	boolean batchInsert(List<Employee> empList);
	
	List<Employee> download();
}
