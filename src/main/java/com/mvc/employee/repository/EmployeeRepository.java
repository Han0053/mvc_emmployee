package com.mvc.employee.repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.mvc.employee.bean.Employee;

@Repository
public class EmployeeRepository {

	private static final String QUERY_EMP_BY_ID = "SELECT * FROM employee WHERE id = :id";

	private static final String DELETE_EMP_by_ID = "DELETE FROM employee WHERE id = :id";

	private static final String UPDATE_EMP = "UPDATE employee SET height = :height, weight = :weight, eng_name = :engName,"
			+ " chi_Name = :chiName, phone = :phone, email = :email, bmi = :bmi WHERE id = :id";

	private static final String INSERT_EMP = "INSERT INTO employee (id, height, weight, eng_name, chi_name, phone, email, bmi) "
			+ "VALUES (:id, :height, :weight, :engName, :chiName, :phone, :email, :bmi)";

	private static final String QUERY_ALL = "SELECT * FROM employee";
	
	private static final String QUERY_PHONE = "SELECT COUNT(*) FROM employee WHERE phone = :phone";
	
	private static final String QUERY_EMAIL = "SELECT COUNT(*) FROM employee WHERE email = :email";

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	public Employee queryId(int id) {
		SqlParameterSource sqlParameterValue = new MapSqlParameterSource().addValue("id", id);

		List<Employee> result = jdbcTemplate.query(QUERY_EMP_BY_ID, sqlParameterValue, new EmployeeRowMapper());
		return result.size() > 0 ? result.get(0) : null;
	}

	public int deleteId(int id) {
		SqlParameterSource sqlParameterValue = new MapSqlParameterSource().addValue("id", id);

		int result = jdbcTemplate.update(DELETE_EMP_by_ID, sqlParameterValue);
		return result;
	}

	public Employee update(Employee employee) {
		SqlParameterSource sqlParameterValue = new MapSqlParameterSource()
				.addValue("id", employee.getId())
				.addValue("height", employee.getHeight())
				.addValue("weight", employee.getWeight())
				.addValue("engName", employee.getEngName())
				.addValue("chiName", employee.getChiName())
				.addValue("phone", employee.getPhone())
				.addValue("email", employee.getEmail())
				.addValue("bmi", employee.getBmi());

		jdbcTemplate.update(UPDATE_EMP, sqlParameterValue);
		return employee;
	}

	public Employee create(Employee employee) {
		SqlParameterSource sqlParameterValue = new MapSqlParameterSource()
				.addValue("id", employee.getId())
				.addValue("height", employee.getHeight())
				.addValue("weight", employee.getWeight())
				.addValue("engName", employee.getEngName())
				.addValue("chiName", employee.getChiName())
				.addValue("phone", employee.getPhone())
				.addValue("email", employee.getEmail())
				.addValue("bmi", employee.getBmi());

		jdbcTemplate.update(INSERT_EMP, sqlParameterValue);
		return employee;
	}

	@Transactional(propagation= Propagation.REQUIRED,rollbackFor= SQLException.class)
	public boolean batchInsert(List<Employee> list) {

		SqlParameterSource[] sqlParameterSources = new SqlParameterSource[list.size()];
		int i = 0;

		for (Employee emp : list) {
			sqlParameterSources[i] = new MapSqlParameterSource()
					.addValue("id", emp.getId())
					.addValue("height", emp.getHeight())
					.addValue("weight", emp.getWeight())
					.addValue("engName", emp.getEngName())
					.addValue("chiName", emp.getChiName())
					.addValue("phone", emp.getPhone())
					.addValue("email", emp.getEmail())
					.addValue("bmi", emp.getBmi());
			i++;
		}

		int[] updateCounts = jdbcTemplate.batchUpdate(INSERT_EMP, sqlParameterSources);
		return updateCounts.length > 0;
	}

	public List<Employee> queryAll() {
		List<Employee> empList = new ArrayList<Employee>();
		
		empList = jdbcTemplate.query(QUERY_ALL, new EmployeeRowMapper());
		
		return empList;
	}
	
	public boolean checkPhoneDao(String phone) {
		SqlParameterSource sqlParameterValue = new MapSqlParameterSource().addValue("phone", phone);
		int count = jdbcTemplate.queryForObject(QUERY_PHONE, sqlParameterValue, Integer.class);	
		return count > 0 ? true : false;
	}
	
	public boolean checkEmailDao(String email) {
		SqlParameterSource sqlParameterValue = new MapSqlParameterSource().addValue("email", email);
		int count = jdbcTemplate.queryForObject(QUERY_EMAIL, sqlParameterValue, Integer.class);	
		return count > 0 ? true : false;
	}
}