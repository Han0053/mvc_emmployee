package com.mvc.employee.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.mvc.employee.bean.Employee;
 
@Repository
public interface EmpRepository extends CrudRepository<Employee, Integer> {
 
}