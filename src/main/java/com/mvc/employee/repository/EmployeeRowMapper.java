package com.mvc.employee.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import com.mvc.employee.bean.Employee;
import com.mvc.employee.service.EmployeeMybatisService;

public class EmployeeRowMapper implements RowMapper<Employee>	{
	
	private static final Logger logger = LogManager.getLogger(EmployeeMybatisService.class);
	
	@Override
	public Employee mapRow(ResultSet rs, int rowNum) {
		// TODO Auto-generated method stub
		Employee emp = new Employee();
        try {
			emp.setId(rs.getInt("id"));
			emp.setHeight(rs.getInt("height"));
	        emp.setWeight(rs.getInt("weight"));
	        emp.setEngName(rs.getString("eng_name"));
	        emp.setChiName(rs.getString("chi_name"));
	        emp.setPhone(rs.getString("phone"));
	        emp.setEmail(rs.getString("email"));
	        emp.setBmi(rs.getFloat("bmi"));
	        emp.setCreateTime(rs.getTimestamp("create_time"));
	        emp.setUpdateTime(rs.getTimestamp("update_time"));
		} catch (SQLException e) {
			logger.error("error: {}",e);
		}
        return emp;
	}  
}
