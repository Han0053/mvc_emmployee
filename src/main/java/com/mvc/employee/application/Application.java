package com.mvc.employee.application;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableCaching
@SpringBootApplication
@MapperScan("com.mvc.employee.mapper")
@ComponentScan("com.mvc.employee.*")
@ComponentScan("com.mvc.employee.controller.*")
@EnableTransactionManagement
@EnableAutoConfiguration(exclude = {ErrorMvcAutoConfiguration.class})
public class Application {
	 public static void main(String[] args) {
	        SpringApplication.run(Application.class, args);
	    }
}
