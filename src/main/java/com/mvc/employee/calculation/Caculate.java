package com.mvc.employee.calculation;

import java.text.DecimalFormat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mvc.employee.service.EmployeeMybatisService;

public class Caculate {
	
	private static final Logger logger = LogManager.getLogger(EmployeeMybatisService.class);
	
	public double bmi(double height, double weight) {
		
		if (height == 0) {
			logger.error("The  is zero");
		}
		
		double bmi = weight / Math.pow((height / 100), 2);
		DecimalFormat df = new DecimalFormat("######0.00");

		return Double.parseDouble(df.format(bmi));
	}
}
